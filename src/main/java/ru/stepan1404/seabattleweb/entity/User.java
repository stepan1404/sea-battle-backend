package ru.stepan1404.seabattleweb.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    @Id
    @Setter(AccessLevel.NONE)
    @SequenceGenerator(name = "userSeq", sequenceName = "user_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userSeq")
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Long winCount = 0L;
}
