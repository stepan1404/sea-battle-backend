package ru.stepan1404.seabattleweb.controller;

import org.springframework.core.convert.ConversionService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.stepan1404.seabattleweb.contract.UserDto;
import ru.stepan1404.seabattleweb.service.UserService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class WebController {
    private final ConversionService conversionService;
    private final UserService userService;

    public WebController(ConversionService conversionService, UserService userService) {
        this.conversionService = conversionService;
        this.userService = userService;
    }

    @GetMapping("/results/{name}")
    public UserDto getUserStat(@PathVariable String name) {
        return conversionService.convert(userService.getByName(name), UserDto.class);
    }

    @GetMapping("/results")
    public List<UserDto> getUserStats() {
        return userService.getAll().stream()
                .map(user -> conversionService.convert(user, UserDto.class))
                .sorted(Collections.reverseOrder(Comparator.comparing(UserDto::getWinCount)))
                .collect(Collectors.toList());
    }

    @PostMapping("/results")
    public UserDto addNewWinToStat(@RequestBody @Validated(UserDto.OnPost.class) UserDto dto) {
        return conversionService.convert(userService.addNewWin(dto.getName()), UserDto.class);
    }
}
