package ru.stepan1404.seabattleweb.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.stepan1404.seabattleweb.contract.UserDto;
import ru.stepan1404.seabattleweb.entity.User;

@Component
public class UserConverter implements Converter<User, UserDto> {
    @Override
    public UserDto convert(User user) {
        return UserDto.builder()
                .name(user.getName())
                .winCount(user.getWinCount())
                .build();
    }
}
