package ru.stepan1404.seabattleweb.contract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserDto {
    @NotEmpty(groups = OnPost.class)
    @Pattern(regexp = "^[A-Za-z0-9-_ ]*$", message = "Name must contains only english symbols, digits or special symbols.")
    private String name;
    private Long winCount;

    public interface OnPost {
    }
}
