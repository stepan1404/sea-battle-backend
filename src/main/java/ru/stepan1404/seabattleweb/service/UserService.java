package ru.stepan1404.seabattleweb.service;

import ru.stepan1404.seabattleweb.entity.User;

import java.util.List;

public interface UserService {
    User getByName(String name);

    User addNewWin(String name);

    List<User> getAll();
}
