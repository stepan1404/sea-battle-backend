package ru.stepan1404.seabattleweb.service.impl;

import org.springframework.stereotype.Service;
import ru.stepan1404.seabattleweb.entity.User;
import ru.stepan1404.seabattleweb.exception.UserNotFoundException;
import ru.stepan1404.seabattleweb.repository.UserRepository;
import ru.stepan1404.seabattleweb.service.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getByName(String name) {
        return userRepository.findByName(name).orElseThrow(() -> new UserNotFoundException(name));
    }

    @Override
    public User addNewWin(String name) {
        User user = userRepository.findByName(name).orElseGet(() -> {
            User newUser = new User();
            newUser.setName(name);
            return newUser;
        });
        user.setWinCount(user.getWinCount() + 1);
        return userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
}
