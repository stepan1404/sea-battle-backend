package ru.stepan1404.seabattleweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.stepan1404.seabattleweb.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByName(String name);
}
